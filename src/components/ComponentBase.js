import '../../style/ComponentBase.less';

import { Component } from 'react';
import PropTypes from 'prop-types';

export default class ComponentBase extends Component {

	constructor( props ) {

		super( props );
		this.bindMethods();
		this.setInitialState();
	}

	bindMethods() {

		//falback.
	}

	setInitialState() {

		//fallback
	}

	render() {

		return (<h1>Extended component has no render function</h1> );
	}

	component_classes( cls, options ) {

		options = options || {};

		cls.push( 'cb' );

		//extraclassnames.
		if( this.props.className )
			cls = cls.concat( this.props.className.split(' ') );

		//position hook ( eg. absolute, fixed )
		if( this.props.position )
			cls.push( 'cb-' + this.props.position );

		if( this.props.align ) {
			cls.push( 'cb-' + this.props.align );
		}

		if( this.props.alignChildren ) {
			cls.push( 'cb-sibs-' + this.props.alignChildren );
		}

		//place a class catch for inversing colours.
		if( this.props.inverse ) {
			cls.push( 'cb-inverse' );
		}

		/* remove animation for prerendition */
		if( this.state && this.state.cb_animate === false ) {
			cls.push( 'cb-no-anim' );
		}

		//can grow, can shrink;
		if( this.props.flexible ) {
			cls.push( 'cb-flexible');
		}

		if( this.props.flexibleContent ) {
			cls.push( 'cb-sibs-flexible' );
		}

		//can grow
		if( this.props.growable ) {
			cls.push( 'cb-grow' );
		}

		//direct siblings can grow... may change this to props.growable, and then props.shrinkable.
		if( this.props.growableContent ) {
			cls.push( 'cb-sibs-grow' );
		}

		//can shrink
		if( this.props.shrinkable ) {
			cls.push( 'cb-shrink' );
		}

		//
		if( this.props.shrinkableContent ) {
			cls.push( 'cb-sibs-shrink' );
		}

		if( this.props.direction === 'row' || options.direction === 'row' ) {
			cls.push( 'cb-row' );
		}
		else if( this.props.direction === 'column' || options.direction === 'column' ){
			cls.push( 'cb-column' );
		}

		else {
			cls.push( 'cb-row' );
		}

		if( this.props.justify && this.props.justify !== 'start' ) {
			cls.push( 'cb-end' );
		}

		if( this.props.devel ) {
			cls.push( 'cb-devel' );
		}

		return cls;
	}

	component_style( style ) {

		style = style || {};

		if( this.props.basis !== 'auto' )
			style.flexBasis = this.props.basis;

		return this.props.style
			? Object.assign( this.props.style, style )
			: style;
	}

	content_classes( cls, options ) {

		options = options || {};
		cls.push( 'cb-content' );

		if( this.props.className )
			cls = cls.concat( this.props.className.split(' ').map( ( cls ) => { return cls + '-content' } ) )

		return cls;
	}

	content_style( style ) {

		style = style || {};
		return style;


		if( this.props.className )
			cls = cls.concat( this.props.className.split(' ').map( ( cls ) => { return cls + '-content' } ) )
	}

}

ComponentBase.propTypes = {

	/**
	 * (deprecated) Component can grow and shrink
	 */
	flexible: PropTypes.bool,

	/**
	 * Component can grow to meet the largest extents of the parent alongside its peer elements.
	 */
	growable: PropTypes.bool,

	/**
	 * Component can shrink
	 */
	shrinkable: PropTypes.bool,

	/**
	 * Useful in container Components, permit the container contents to grow.
	 */
	growableContent: PropTypes.bool,

	/**
	 * Useful in container Components, permit the container contents to shrink.
	 */
	shrinkableContent: PropTypes.bool,

	/**
	 * For completeness the basis property is added here for css flex-basis ( https://developer.mozilla.org/en-US/docs/Web/CSS/flex-basis?v=control )
	 */
	basis: PropTypes.string,

	/**
	* Content can grow AND shrink.
	**/
	flexibleContent: PropTypes.bool,


	/**
	 * The primary axis for this flexbox, defined as row (horizontal) and column(vertical)
	 */
	direction: PropTypes.string,

	/**
	* The alignment on the primary axis, defined as 'start' and 'end'
	*/
	justify: PropTypes.string,

	/**
	* Render this component with surrounding development marking.
	*/
	devel: PropTypes.bool

}

ComponentBase.defaultProps = {
	flexible: false,
	growable: false,
	growableContent: false,
	shrinkable: false,
	shrinkableContent: false,
	basis: 'auto',
	direction: 'column',
	alignment: 'start',
	devel: false
}
