`ComponentBase.js` (component)
repo: [git@bitbucket.org:pielio/componentbase.git](https://bitbucket.org/pielio/componentbase)

Props
-----

### `alignment`

defaultValue: `'start'`


### `basis`

For completeness the basis property is added here for css flex-basis ( https://developer.mozilla.org/en-US/docs/Web/CSS/flex-basis?v=control )

type: `string`
defaultValue: `'auto'`


### `devel`

Render this component with surrounding development marking.

type: `bool`
defaultValue: `false`


### `direction`

The primary axis for this flexbox, defined as row (horizontal) and column(vertical)

type: `string`
defaultValue: `'column'`


### `flexible`

(deprecated) Component can grow and shrink

type: `bool`
defaultValue: `false`


### `flexibleContent`

Content can grow AND shrink.

type: `bool`


### `growable`

Component can grow to meet the largest extents of the parent alongside its peer elements.

type: `bool`
defaultValue: `false`


### `growableContent`

Useful in container Components, permit the container contents to grow.

type: `bool`
defaultValue: `false`


### `justify`

The alignment on the primary axis, defined as 'start' and 'end'

type: `string`


### `shrinkable`

Component can shrink

type: `bool`
defaultValue: `false`


### `shrinkableContent`

Useful in container Components, permit the container contents to shrink.

type: `bool`
defaultValue: `false`

