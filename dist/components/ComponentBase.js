'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.default = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

require('../../style/ComponentBase.less');

var _react = require('react');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ComponentBase = function (_Component) {
	_inherits(ComponentBase, _Component);

	function ComponentBase(props) {
		_classCallCheck(this, ComponentBase);

		var _this = _possibleConstructorReturn(this, (ComponentBase.__proto__ || Object.getPrototypeOf(ComponentBase)).call(this, props));

		_this.bindMethods();
		_this.setInitialState();
		return _this;
	}

	_createClass(ComponentBase, [{
		key: 'bindMethods',
		value: function bindMethods() {

			//falback.
		}
	}, {
		key: 'setInitialState',
		value: function setInitialState() {

			//fallback
		}
	}, {
		key: 'render',
		value: function render() {

			return React.createElement(
				'h1',
				null,
				'Extended component has no render function'
			);
		}
	}, {
		key: 'component_classes',
		value: function component_classes(cls, options) {

			options = options || {};

			cls.push('cb');

			//extraclassnames.
			if (this.props.className) cls = cls.concat(this.props.className.split(' '));

			//position hook ( eg. absolute, fixed )
			if (this.props.position) cls.push('cb-' + this.props.position);

			if (this.props.align) {
				cls.push('cb-' + this.props.align);
			}

			if (this.props.alignChildren) {
				cls.push('cb-sibs-' + this.props.alignChildren);
			}

			//place a class catch for inversing colours.
			if (this.props.inverse) {
				cls.push('cb-inverse');
			}

			/* remove animation for prerendition */
			if (this.state && this.state.cb_animate === false) {
				cls.push('cb-no-anim');
			}

			//can grow, can shrink;
			if (this.props.flexible) {
				cls.push('cb-flexible');
			}

			if (this.props.flexibleContent) {
				cls.push('cb-sibs-flexible');
			}

			//can grow
			if (this.props.growable) {
				cls.push('cb-grow');
			}

			//direct siblings can grow... may change this to props.growable, and then props.shrinkable.
			if (this.props.growableContent) {
				cls.push('cb-sibs-grow');
			}

			//can shrink
			if (this.props.shrinkable) {
				cls.push('cb-shrink');
			}

			//
			if (this.props.shrinkableContent) {
				cls.push('cb-sibs-shrink');
			}

			if (this.props.direction === 'row' || options.direction === 'row') {
				cls.push('cb-row');
			} else if (this.props.direction === 'column' || options.direction === 'column') {
				cls.push('cb-column');
			} else {
				cls.push('cb-row');
			}

			if (this.props.justify && this.props.justify !== 'start') {
				cls.push('cb-end');
			}

			if (this.props.devel) {
				cls.push('cb-devel');
			}

			return cls;
		}
	}, {
		key: 'component_style',
		value: function component_style(style) {

			style = style || {};

			if (this.props.basis !== 'auto') style.flexBasis = this.props.basis;

			return this.props.style ? Object.assign(this.props.style, style) : style;
		}
	}, {
		key: 'content_classes',
		value: function content_classes(cls, options) {

			options = options || {};
			cls.push('cb-content');

			if (this.props.className) cls = cls.concat(this.props.className.split(' ').map(function (cls) {
				return cls + '-content';
			}));

			return cls;
		}
	}, {
		key: 'content_style',
		value: function content_style(style) {

			style = style || {};
			return style;

			if (this.props.className) cls = cls.concat(this.props.className.split(' ').map(function (cls) {
				return cls + '-content';
			}));
		}
	}]);

	return ComponentBase;
}(_react.Component);

exports.default = ComponentBase;


ComponentBase.propTypes = {

	/**
  * (deprecated) Component can grow and shrink
  */
	flexible: _propTypes2.default.bool,

	/**
  * Component can grow to meet the largest extents of the parent alongside its peer elements.
  */
	growable: _propTypes2.default.bool,

	/**
  * Component can shrink
  */
	shrinkable: _propTypes2.default.bool,

	/**
  * Useful in container Components, permit the container contents to grow.
  */
	growableContent: _propTypes2.default.bool,

	/**
  * Useful in container Components, permit the container contents to shrink.
  */
	shrinkableContent: _propTypes2.default.bool,

	/**
  * For completeness the basis property is added here for css flex-basis ( https://developer.mozilla.org/en-US/docs/Web/CSS/flex-basis?v=control )
  */
	basis: _propTypes2.default.string,

	/**
 * Content can grow AND shrink.
 **/
	flexibleContent: _propTypes2.default.bool,

	/**
  * The primary axis for this flexbox, defined as row (horizontal) and column(vertical)
  */
	direction: _propTypes2.default.string,

	/**
 * The alignment on the primary axis, defined as 'start' and 'end'
 */
	justify: _propTypes2.default.string,

	/**
 * Render this component with surrounding development marking.
 */
	devel: _propTypes2.default.bool

};

ComponentBase.defaultProps = {
	flexible: false,
	growable: false,
	growableContent: false,
	shrinkable: false,
	shrinkableContent: false,
	basis: 'auto',
	direction: 'column',
	alignment: 'start',
	devel: false
};