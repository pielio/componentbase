'use strict';

var _ComponentBase = require('./components/ComponentBase');

var _ComponentBase2 = _interopRequireDefault(_ComponentBase);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

module.exports = _ComponentBase2.default;